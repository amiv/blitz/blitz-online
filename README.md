# blitz-online

This is a web app to view the blitz online. A version of this tool exists already (in plain php/html), but it is very hacky and breaks all the time. This is a reiteration with up-to-date technologies in order to have a stable and convergent application.

## must have

```
[x] reading articles on-site
[ ] reading editions
[x] tree-like structure for articles
[x] database connection for all articles
```

## should have

```
[x] appealing design
[-] convergent design
[x] upload form
[ ] login/permissions
```

## nice to have

```
[x] share separate articles with friends
[ ] integration with redsys
[ ] form to contact the author
```


