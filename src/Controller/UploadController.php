<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Issue;
use App\Repository\ArticleRepository;
use App\Repository\IssueRepository;
use App\Form\UploadType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UploadController extends AbstractController
{
    /**
     * @Route("/upload", name="upload")
     */
    public function index(Request $request, IssueRepository $ir, ArticleRepository $ar): Response
    {
        $form = $this->createForm(UploadType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $data = $form->getData();

            $file = $request->files->get('upload')['html_export'];
            $filename = md5(uniqid()) . '.' . 'html';
            $file->move($this->getParameter('uploads_dir'), $filename);

            $pictures = $request->files->get('upload')['images'];

            $foldername = $data['year'] . '_' . $data['issue'];
            foreach($pictures as $picture) {
                $picture->move($this->getParameter('media_dir') . $foldername, $picture->getClientOriginalName());
            }

            $filecontents = file_get_contents($this->getParameter('uploads_dir') . $filename);
            $articles = explode('<div class="article">', $filecontents);

            $issue = new Issue;

            $em = $this->getDoctrine()->getManager();

            // overwrite existing issues with the same issue number and year
            $existingIssues = $ir->findIssue($data['year'], $data['issue']);
            foreach($existingIssues as $existingIssue) {
                $existingArticles = $ar->findArticlesByIssue($existingIssue);
                foreach($existingArticles as $existingArticle) {
                    $em->remove($existingArticle);
                }
                $em->remove($existingIssue);
            }

            $issue->setTitle($data['title']);
            $issue->setYear($data['year']);
            $issue->setSemester($data['semester']);
            $issue->setNumber($data['issue']);

            $em->persist($issue);


            // index 0 is just metadata
            for($i = 1; $i < count($articles); $i++) {
                $article_entity = new Article;
                $article = $articles[$i];
                $title = $this->string_between($article, '<h1 class="title">', '</h1>');
                $intro = str_replace(PHP_EOL, '', $this->string_between($article, '<div class="intro">', '</div>'));
                $body = str_replace(PHP_EOL, '', $this->string_between($article, '<div class="main">', '</div><div class="article"'));
                $footnotes = $this->string_between($article, '<ol class="footnotes">', '</ol>');
                $match = '';
                $regex = preg_match('/img src="(.*?)files/', $body, $match);
                if($regex) {
                    $body = str_replace($match[0], 'img src="/media/' . $foldername, $body);
                }
                $authorstring = $this->string_between($article, '<div class="author"><i>', '</i></div>');
                $author = '';
                $email = '';
                foreach(explode(', ', $authorstring) as $str) {
                    if(strpos($str, '@') !== false) {
                        $email = $email . ', ' . $str;
                    } else {
                        $author = $author . ', ' . $str;
                    }
                }
                $author = substr($author, 2);
                $article_entity->setTitle($title);
                $article_entity->setAbstract($intro);
                $article_entity->setBody($body);
                $article_entity->setAuthor($author);
                $article_entity->setIssue($issue);
                $article_entity->setFootnotes($footnotes);

                if($email != '') {
                    $email = substr($email, 2);
                    $article_entity->setEmail($email);
                }

                $em->persist($article_entity);
            }


            $em->flush();

            return new RedirectResponse('/');
        }

        return $this->render('upload/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function string_between(string $text, string $start, string $stop) : string 
    {
        $firstexplode = explode($start, $text);
        if(count($firstexplode) > 1) {
            return explode($stop, $firstexplode[1])[0]; 
        } else {
            return '';
        }
    }
}
